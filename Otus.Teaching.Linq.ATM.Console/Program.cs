﻿using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

			MainLoop(atmManager);
            
            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

		static void MainLoop(ATMManager atmManager)
		{
			User authorizedUser = null;
			do
			{
				PrintMainMenu(authorizedUser == null);
				switch (GetUserChoise(5))
				{
					case 1:
						AuthorizeUser(atmManager, out authorizedUser);
						break;
					case 2:
						PrintUserAccountsInfo(atmManager, authorizedUser);
						break;
					case 3:
						PrintUserAccountsInfoWithHistory(atmManager, authorizedUser);
						break;
					case 4:
						PrintInputCashOperations(atmManager);
						break;
					case 5:
						PrintRichUsers(atmManager);
						break;
					case 0:
						return;
				}

				PrintContinueMenu();
				switch (GetUserChoise(1))
				{
					case 1:
						break;
					case 0:
						return;
				}

			} while (true);
		}

		static void PrintMainMenu(bool authorized)
		{
			System.Console.Write("\nВыберите операцию:\n\n" +
				"\t1 - Вывод информации об аккаунте по логину и паролю\n" +
				"\t2 - Вывод данных о счетах пользователя" + (authorized ? " (требуется авторизация командой №1)" : "") + "\n" +
				"\t3 - Вывод данных о счетах пользователя с историей операций" + (authorized ? " (требуется авторизация командой №1)" : "") + "\n" +
				"\t4 - Вывод данных о всех операциях пополнений\n" +
				"\t5 - Вывод данных о всех пользователях с суммой средств на счетах больше заданной\n" +
				"\t0 - Выход из приложения");
		}

		static void PrintContinueMenu()
		{
			System.Console.Write("\nВыберите операцию:\n\n" +
				"\t1 - Вернуться в главное меню\n" +
				"\t0 - Выход из приложения");
		}

		static int GetUserChoise(int maxChoise)
		{
			System.Console.Write("\n\nВаш выбор: ");
			int userChoise = -1;
			int attemptCounter = 0;
			do
			{
				if (attemptCounter > 0)
					System.Console.Write($"\nНеобходимо ввести число от 0 до {maxChoise}! Ваш выбор: ");
				attemptCounter++;
			} while (!int.TryParse(System.Console.ReadLine(), out userChoise) || userChoise < 0 || userChoise > maxChoise);
			return userChoise;
		}

		static void AuthorizeUser(ATMManager atmManager, out User authorizedUser)
		{
			System.Console.Write("Введите логин: ");
			string login;
			int attemptCounter = 0;
			do
			{
				if (attemptCounter > 0)
					System.Console.Write("\nПользователь с данным логином не существует. Введите логин: ");
				login = System.Console.ReadLine();
				attemptCounter++;
			} while (!atmManager.CheckUserNameExist(login));

			System.Console.Write("Введите пароль: ");
			string password;
			attemptCounter = 0;
			int attemptsAllowed = 3;
			do
			{
				if (attemptCounter > 0)
					System.Console.Write($"Неправильный пароль! Осталось попыток: {attemptsAllowed - attemptCounter}. Введите пароль: ");
				password = System.Console.ReadLine();
				attemptCounter++;
			} while ((authorizedUser = atmManager.GetUserInfo(login, password)) == null && attemptCounter < attemptsAllowed);

			if (authorizedUser != null)
			{
				PrintUserInformation(authorizedUser);
			}
			else
			{
				System.Console.Write("\nВы исчерпали попытки ввода пароля.");
			}
		}

		static void PrintUserInformation(User user)
		{
			System.Console.Write($"\nИнформация о пользователе {user.Login}:\n\n" +
				$"\tID: {user.Id}\n" +
				$"\tИмя: {user.FirstName}, Отчество: {user.MiddleName}, Фамилия: {user.SurName}\n" +
				$"\tТелефон: {user.Phone}\n" +
				$"\tСерия и номер паспорта: {user.PassportSeriesAndNumber}\n" +
				$"\tДата регистрации: {user.RegistrationDate}\n");
		}

		static void PrintUserAccountsInfo(ATMManager atmManager, User authorizedUser)
		{
			if (authorizedUser == null)
			{
				System.Console.Write("\nВы не авторизованы! Воспользуйтесь командой №1 в главном меню.");
				return;
			}

			System.Console.Write($"\nУ пользователя {authorizedUser.Login} открыты следующие счета:\n\n");

			foreach (var account in atmManager.GetAccountsByUser(authorizedUser))
			{
				System.Console.Write($"\tID: {account.Id}, Дата открытия: {account.OpeningDate}, Сумма средств: {account.CashAll}\n");
			}
		}

		static void PrintUserAccountsInfoWithHistory(ATMManager atmManager, User authorizedUser)
		{
			if (authorizedUser == null)
			{
				System.Console.Write("\nВы не авторизованы! Воспользуйтесь командой №1 в главном меню.");
				return;
			}

			System.Console.Write($"\nУ пользователя {authorizedUser.Login} открыты следующие счета:\n");

			foreach (var accountWithHistory in atmManager.GetAccountsWithHistoryByUser(authorizedUser))
			{
				System.Console.Write($"\n\tID: {accountWithHistory.Key.Id}, Дата открытия: {accountWithHistory.Key.OpeningDate}, Сумма средств: {accountWithHistory.Key.CashAll}\n");
				foreach (var historyElement in accountWithHistory.Value)
				{
					System.Console.Write($"\t\tID операции: {historyElement.Id}, Дата операции: {historyElement.OperationDate}, Тип операции: {historyElement.OperationType}, Сумма: {historyElement.CashSum}\n");
				}
			}
		}

		static void PrintInputCashOperations(ATMManager atmManager)
		{
			System.Console.Write("\nОперации пополнения:\n");
			foreach (var item in atmManager.GetInputCashOperationsHistory())
			{
				System.Console.Write($"\nID пользователя: {item.Key.Id}, {item.Key.FirstName} '{item.Key.MiddleName}' {item.Key.SurName}:\n");
				foreach (var operation in item.Value)
				{
					System.Console.Write($"\tID операции: {operation.Id}, Дата: {operation.OperationDate}, Сумма пополнения: {operation.CashSum}, ID счёта: {operation.AccountId}\n");
				}
			}
		}

		static void PrintRichUsers(ATMManager atmManager)
		{
			System.Console.Write("Введите сумму: ");
			string strSum;
			decimal sum;
			int attemptCounter = 0;
			do
			{
				if (attemptCounter > 0)
					System.Console.Write("Сумма должна быть числом! Введите сумму: ");
				strSum = System.Console.ReadLine();
				attemptCounter++;
			} while (!decimal.TryParse(strSum, out sum));
			
			System.Console.Write($"\nСледующие пользователи располагают суммой на счетах больше {sum}:");
			foreach (var user in atmManager.GetRichUsers(sum))
			{
				PrintUserInformation(user);
			}
		}
	}
}