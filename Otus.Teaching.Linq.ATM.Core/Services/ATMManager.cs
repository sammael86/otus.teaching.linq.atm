﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

		/// <summary>
		/// Проверка на наличие пользователя по логину
		/// </summary>
		public bool CheckUserNameExist(string login)
		{
			return Users.Any(x => x.Login == login);
		}

		/// <summary>
		/// Получение информации о пользователе по логину и паролю
		/// </summary>
		public User GetUserInfo(string login, string password)
		{
			return Users.Where(x => x.Login == login && x.Password == password).FirstOrDefault();
		}

		/// <summary>
		/// Получение списка всех счетов пользователя
		/// </summary>
		public List<Account> GetAccountsByUser(User user)
		{
			return Accounts.Where(x => x.UserId == user.Id).ToList();
		}

		/// <summary>
		/// Получение списка всех счетов пользователя с отображением истории операций по каждому из них
		/// </summary>
		public Dictionary<Account, IEnumerable<OperationsHistory>> GetAccountsWithHistoryByUser(User user)
		{
			return GetAccountsByUser(user).GroupJoin(
				History,
				a => a.Id,
				o => o.AccountId,
				(account, operations) => new
				{
					Account = account,
					Operations = operations
				}).ToDictionary(a => a.Account, h => h.Operations);
		}

		/// <summary>
		/// Получение списка всех операций пополнения, сгруппированных по пользователям
		/// </summary>
		public Dictionary<User, IEnumerable<OperationsHistory>> GetInputCashOperationsHistory()
		{
			return Users.GroupJoin(
				History.Where(x => x.OperationType == OperationType.InputCash),
				u => u.Id,
				o => o.AccountId,
				(user, operations) => new
				{
					User = user,
					Operations = operations
				}).ToDictionary(u => u.User, o => o.Operations);
		}

		/// <summary>
		/// Получение списка пользователей с суммой средств на счетах больше заданной
		/// </summary>
		public List<User> GetRichUsers(decimal Sum)
		{
			return Users.GroupJoin(
				Accounts,
				u => u.Id,
				a => a.UserId,
				(user, sum) => new
				{
					User = user,
					Sum = sum.Sum(x => x.CashAll)
				}).Where(x => x.Sum > Sum).Select(x => x.User).ToList();
		}
	}
}